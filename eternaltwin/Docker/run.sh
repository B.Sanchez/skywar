#!/bin/sh

if [ -e ./eternaltwin.toml ]; then
	eternaltwin start --config ./eternaltwin.toml "$@"
else
	eternaltwin start "$@"
fi
